import { Component } from '@angular/core';
import { PageTitle } from 'src/app/decorators';
// import { PageTitle } from 'src/app/decorators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
@PageTitle(['Web', 'Home'])
export class HomeComponent {}
