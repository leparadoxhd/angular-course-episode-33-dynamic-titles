import { Component } from '@angular/core';
import { PageTitle } from 'src/app/decorators';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss'],
})
@PageTitle(['Web', 'Demo'])
export class DemoComponent {}
