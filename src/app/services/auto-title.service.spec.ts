import { TestBed } from '@angular/core/testing';

import { AutoTitleService } from './auto-title.service';

describe('AutoTitleService', () => {
  let service: AutoTitleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutoTitleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
